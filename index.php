<?php
 
require_once("animal.php");
$animal = new Animal("shaun");

echo " Name: " . $animal->name . "<br>" ;
echo " legs: " . $animal->legs . "<br>" ;
echo " Cold Blooded : " . $animal->cold_blooded . "<br>  <br>" ;

require_once("Frog.php");
$kodok = new Frog("buduk");

echo " Name: " . $kodok->name . "<br>" ;
echo " legs: " . $kodok->legs . "<br>" ;
echo " Cold Blooded : " . $kodok->cold_blooded . "<br> ";
echo " Jump : " . $kodok->Jump . "<br> <br>" ;


require_once("Ape.php");
$sungokong = new Ape("Kera Sakti");

echo " Name: " . $sungokong->name . "<br>" ;
echo " legs: " . $sungokong->legs . "<br>" ;
echo " Cold Blooded : " . $sungokong->cold_blooded . "<br> " ;
echo " Yell : " . $sungokong->Yell . "<br> " ;

?>